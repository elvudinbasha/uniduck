
# Uniduck
[https://bitbucket.org/elvudinbasha/uniduck/](https://bitbucket.org/elvudinbasha/uniduck/)
## Description

Uniduck project - an assignment from [Infinum](https://infinum.com)

## Getting Started

### Dependencies

* Wordpress
* Gulp
* Sass for CSS
* Auto Image optimization
* Custom architecture design


### Installing

Clone **Uniduck** theme  into your  `wp-content/themes` directory  .

In Terminal,  `cd`  into the  `wp-content`  directory and install the Gulp packages (if you haven't already installed Gulp, you’ll need to  [do so](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)  first):
```
`npm install`
```
Once you have installed the packages, in Terminal while still in the  `wp-content/gulp`  directory, run  `gulp watch`.

SCSS files in  `wp-content/gulp/scss/`  are compiled and minified over to  `uniduck/css`.

JavaScript files in  `wp-content/gulp/scripts/`  are uglified, concatenated and sent over to  `uniduck/js/`.

## Authors

[Elvudin Basha](elvudin7@gmail.com)
[@elvudinbasha](https://www.linkedin.com/in/elvudinbasha/)


## License

This project is licensed under the GPL v2 License - see the LICENSE.md file for details

