<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'admin_uniduck' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ry2nQ`yN4{$lGTRj.4do<d+$3{{[P6ZHEVdX9{ri: Ug}5a1YwjYK/XiYn4L_&}E' );
define( 'SECURE_AUTH_KEY',  'qaB7]T7^/#a:AXo9k:ao}v9vW>Zoj+7$:Gf>MoO-:4>bT9O{MGa=*7VlA9Xe-//[' );
define( 'LOGGED_IN_KEY',    'V<yZAp9{hN:l(7&%zYgEUQJ]JCSRi]z(CZ=T6H>t{!NBwd`&9]B#<@O2+KD:l/1+' );
define( 'NONCE_KEY',        'SC&3@^5C6mF}iCz+fr=3@kz2Zg@%ZH~ 8Yb{5+/w^M`kES./R%_dq&4dhk{*^FcH' );
define( 'AUTH_SALT',        '}mR+U)FZf;1BXud[i=J:hai[z<)xl_A?G&69]WVQ2U:oPbGpoPRM,hJ;gHFm2if-' );
define( 'SECURE_AUTH_SALT', 'o@G,QXSVBKSG9OG=hj.&+r;G6gpIjSJ9WG`40yN1&$;<vDk**E*ABp^y<kr!c&])' );
define( 'LOGGED_IN_SALT',   'K2ZbsD<l=hm2I;H==:y> WFf~[{0(}X(es*c|;}{z<+}3RC.6-qu|5VsOq|D}I/!' );
define( 'NONCE_SALT',       'B>/RJxxjIwa{QGPT&-xooa8D?g3$5>xm0iFli:M(D:xJ.q3)g>pwc^%ND#hckeL.' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'uniduck_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
