
jQuery(window).on('load', function () {
    jQuery('#loader-wrapper').fadeOut('2000');
});

var windowSize = jQuery(window).width();
jQuery(document).on('click', ".navbar-toggler", function () {
    jQuery("html").toggleClass("menu-open");
});
jQuery(document).on('click', '.menu-item:not(.menu-item-has-children) .nav-link', function () {
    setTimeout(function () {
        jQuery("html").removeClass("menu-open");
    }, 300);
});

(function (jQuery) {
    jQuery.fn.visible = function (partial) {
        var $t = jQuery(this),
            $w = jQuery(window),
            viewTop = $w.scrollTop(),
            viewBottom = viewTop + $w.height(),
            _top = $t.offset().top,
            _bottom = _top + $t.height(),
            compareTop = partial === true ? _bottom : _top,
            compareBottom = partial === true ? _top : _bottom;
        return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
    };
})(jQuery);
var win = jQuery(window);
var allMods = jQuery(".module");
jQuery(document).ready(function () {
    allMods.each(function (i, el) {
        var el = jQuery(el);
        if (el.visible(true)) {
            el.addClass("already-visible");
        }
    });
});
var previousScroll = 0;
jQuery(window).on("load scroll", function () {
    var currentScroll = jQuery(this).scrollTop();
    if (currentScroll > previousScroll && currentScroll >= 1) {
        // scroll down
        allMods.each(function (i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                el.addClass("come-in");
            }
        });
        if (windowSize <= 767) {
            jQuery('html:not(.menu-open) body').addClass('scrolling-down');
        }
    } else {
        // scroll up
        allMods.each(function (i, el) {
            var el = jQuery(el);
            if (el.visible(true)) {
                el.addClass("come-in");
            }
        });
        if (windowSize <= 767) {
            jQuery('html:not(.menu-open) body').removeClass('scrolling-down');
        }
    }
    previousScroll = currentScroll;
});
jQuery('.search_section').on('click', function () {
    jQuery('.search-wrapper').removeClass('d-none');
});
jQuery('.close_search').on('click', function () {
    jQuery('.search-wrapper').addClass('d-none');
});
jQuery(function () {
    var load_more_counter = 3;
    jQuery('.load_more--button').on('click', function () {
        jQuery.ajax({
            type: "POST",
            url: ajax.ajax_url,
            data: {
                action: 'load_more_function',
                paged: load_more_counter,
                posts_per_page: 3
            },
            beforeSend: function () {
                jQuery(".load_more--button").text('Loading...');
            },
            success: function (data) {
                if (data != 0) {
                    jQuery(data).appendTo('.post_section--wrapper');
                    load_more_counter++;
                } else {
                    jQuery(".load_more--button").hide();
                }
            },
            complete: function () {
                jQuery(".load_more--button").text('Load More');
            },
            error: function (errorThrown) {
                alert(errorThrown);
            }
        });
    });
});
