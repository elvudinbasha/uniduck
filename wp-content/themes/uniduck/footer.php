<footer class="footer block--padding">
    <div class="container">
        <div class="footer_wrapper">
            <div class="footer_wrapper--left">
                <a class="footer_logo" href="<?= home_url() ?>">
                    <img srcset="<?= get_template_directory_uri(); ?>/images/Logo.png 1x, <?= get_template_directory_uri(); ?>/images/Logo@2x.png 2x" src="<?= get_template_directory_uri(); ?>/images/Logo.png" alt="logo">
                </a>
                <p>© <?= date('Y') ?> Uniduck. All rights reserved</p>
            </div>
            <div class="footer_wrapper--right">
                <a href="https://facebook.com/">Like <span>Uniduck</span> on <img src="<?= get_template_directory_uri() ?>/images/ic-facebook.svg"/> </a>
                <a href="https://twitter.com/">Follow <span>@uniduck</span> on <img src="<?= get_template_directory_uri() ?>/images/ic-twitter.svg"/> </a>
                <a href="https://instagram.com/">Follow <span>@uniduck</span> on <img src="<?= get_template_directory_uri() ?>/images/ic-instagram.svg"/> </a>
            </div>
        </div>
    </div>
</footer>
<?php
wp_footer();
?>
</html>