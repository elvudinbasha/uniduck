<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title><?php
        global $page, $paged;
        wp_title('|', true, 'right');
        bloginfo('name');
        $site_description = get_bloginfo('description', 'display');
        if ($site_description && (is_home() || is_front_page()))
            echo " | $site_description";
        ?>
    </title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="loader-wrapper"></div>
<div class="search-wrapper module d-none">
    <div class="close_search">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <path fill="#fff" d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/>
        </svg>
    </div>
    <?php get_search_form(); ?>
</div>
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-right">
            <div class="module">
                <a class="navbar__logo" href="<?= home_url() ?>">
                    <img srcset="<?= get_template_directory_uri(); ?>/images/Logo.png 1x, <?= get_template_directory_uri(); ?>/images/Logo@2x.png 2x" src="<?= get_template_directory_uri(); ?>/images/Logo.png" alt="logo">
                </a>
            </div>
            <button class="navbar-toggler" type="button">
                    <span class="navbar-toggler-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
            </button>
            <div class="navbar-collapse">
                <ul class="navbar-nav module ml-auto">
                    <?php
                    echo wp_nav_menu_no_ul();
                    ?>
                    <div class="button_links module">
                        <a class="button_links--pink" href="#">
                            <svg width="17px" height="20px" viewBox="0 0 17 20" version="1.1" fill="#fff"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <path d="M9.38601811,1.76681967 C10.7540905,0.0384262295 12.6560724,0.03 12.6560724,0.03 C12.6560724,0.03 12.9389155,1.65498361 11.5798732,3.22036066 C10.1286821,4.89183607 8.47920322,4.61829508 8.47920322,4.61829508 C8.47920322,4.61829508 8.16947485,3.30377049 9.38601811,1.76681967" id="Fill-1"></path>
                                <path d="M8.63285312,5.72734426 C9.33672837,5.72734426 10.6428893,4.8 12.3431288,4.8 C15.269831,4.8 16.4211449,6.79616393 16.4211449,6.79616393 C16.4211449,6.79616393 14.1693119,7.8997377 14.1693119,10.5774754 C14.1693119,13.5982623 16.9744487,14.6392787 16.9744487,14.6392787 C16.9744487,14.6392787 15.0135312,19.9297049 12.3649175,19.9297049 C11.14834,19.9297049 10.2026338,19.1438689 8.92082696,19.1438689 C7.614666,19.1438689 6.31835614,19.9590492 5.47417103,19.9590492 C3.05555332,19.9590492 0,14.9405574 0,10.9065902 C0,6.93763934 2.58632596,4.85560656 5.01219517,4.85560656 C6.58919316,4.85560656 7.81302213,5.72734426 8.63285312,5.72734426" id="Fill-2"></path>
                            </svg>
                            <?php _e('Get for ios', 'uniduck'); ?>
                        </a>
                        <a class="button_links--gray" href="#">
                            <svg width="21px" height="19px" viewBox="0 0 21 19" version="1.1"
                                 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <g fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1114 -49)">
                                        <g transform="translate(153 31)">
                                            <g transform="translate(931)">
                                                <g transform="translate(28 15)">
                                                    <polygon points="0 0 24 0 24 24 0 24" opacity=".75"/>
                                                    <path d="m22.413 14.3v6.2699c-0.55129 0.014072-0.79498-0.41827-1.1027-0.75615-0.15114-0.16594-0.29361-0.48417-0.5401-0.32812-0.86461 0.54687-1.9908 0.42036-2.7859 1.1899-0.65237 0.63117-1.4953 1.0262-2.3922 1.2437-1.1114 0.26933-1.7935-0.13376-2.1045-1.2285-0.14583-0.51315-0.25936-1.0352-0.39693-1.5507-0.074381-0.2788-0.1949-0.57181-0.53493-0.55328-1.4945 0.081648-3.046-0.11968-4.3572 0.89422-0.45817 0.35404-0.95675 0.66335-1.4626 0.94745-1.7818 1.0005-4.1715-0.38246-4.2101-2.4094-0.019714-1.0312 0.43804-1.8943 0.94249-2.7253 1.072-1.766 1.978-3.6312 3.1832-5.3164 0.24118-0.33718 0.26131-0.5941-0.041385-0.91164-0.41595-0.43652-0.77373-0.92738-1.1768-1.3767-1.1462-1.2779-2.1712-2.6636-3.4331-3.8386v-0.41799c0.41105-0.083598 0.71012 0.15661 1.0258 0.35083 1.6037 0.98702 3.2037 1.98 4.8138 2.9567 0.59141 0.35864 0.84364 0.21763 0.89817-0.46313 0.076059-0.94773 0.12332-1.8992 0.37372-2.8235 0.047397-0.17514 0.07508-0.39305 0.29221-0.43457 0.2272-0.043471 0.30773 0.17082 0.41301 0.31558 0.42378 0.58157 0.84168 1.1674 1.2634 1.7505 0.11213 0.15535 0.24565 0.29914 0.45747 0.26027 0.2532-0.046676 0.20021-0.27462 0.22958-0.44837 0.069767-0.4113 0.12052-0.82679 0.21308-1.2329 0.058862-0.25902 0.063615-0.63172 0.40015-0.6621 0.3178-0.028702 0.42713 0.32032 0.55506 0.55607 0.57743 1.0645 0.92096 2.2042 1.0067 3.4681 0.3747-0.48348 0.30675-0.90788 0.087664-1.3564-0.10878-0.22237-0.23139-0.55133 0.21503-0.54395 1.7937 0.029817 3.435 0.43234 4.4784 2.0645 0.60372 0.94438 1.3303 1.7866 2.06 2.6296 0.81246 0.93825 1.3644 1.9831 1.3945 3.2578 0.009787 0.41604 0.060679 0.83361 0.23517 1.2228" fill="#fff"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <?php _e('Unicorn owners', 'uniduck'); ?>
                        </a>
                    </div>
                </ul>
            </div>
        </nav>
    </div>
</header>