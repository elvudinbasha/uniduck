<?php
get_header();
?>
    <div class="block block--padding not__found">
        <div class="container text-center">
            <h2 class="block__title mb-0">Page not found</h2>
        </div>
    </div>
<?php
get_footer();
?>