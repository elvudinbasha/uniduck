<?php
get_header();
?>
    <div class="container">
        <div class="block--padding main_section module text-center">
            <h1 class="main_section--title"><?php _e('The unicorn & a Duck', 'unicorn'); ?></h1>
            <div class="search_section">
        <span class="search_button">
             <svg width="17px" height="18px" viewBox="0 0 17 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <path d="M10.6745175,13.3066377 C9.60576138,13.9869658 8.34544968,14.3796992 6.99588477,14.3796992 C3.1321643,14.3796992 0,11.1606939 0,7.18984962 C0,3.21900532 3.1321643,0 6.99588477,0 C10.8596052,0 13.9917695,3.21900532 13.9917695,7.18984962 C13.9917695,8.8673557 13.4327757,10.4106819 12.4957824,11.6338158 C12.5514367,11.6746811 12.6044783,11.7206936 12.65425,11.7718452 L16.0122747,15.222973 C16.4980461,15.7222128 16.488377,16.5219413 15.9906781,17.0092167 C15.4929793,17.4964921 14.695719,17.486793 14.2099476,16.9875533 L10.8519229,13.5364255 C10.7827483,13.465333 10.7236203,13.388147 10.6745175,13.3066377 Z M12.2491602,7.18984962 C12.2491602,4.20810544 9.89718867,1.79092418 6.99588477,1.79092418 C4.09458087,1.79092418 1.74260936,4.20810544 1.74260936,7.18984962 C1.74260936,10.1715938 4.09458087,12.5887751 6.99588477,12.5887751 C9.89718867,12.5887751 12.2491602,10.1715938 12.2491602,7.18984962 Z" id="Combined-Shape"></path>
        </svg>
        </span>
                <h4><?php _e('Search blog', 'uniduck'); ?></h4>
            </div>
        </div>
        <div class="post_section block--padding">
            <div class="row post_section--wrapper">
                <?php
                $args = array(
                    'posts_per_page' => 7
                );
                $post_query = new WP_Query($args);
                $postCount = 0;
                if ($post_query->have_posts()) {
                    while ($post_query->have_posts()) {
                        $post_query->the_post();
                        $postCount++;
                        if ($postCount == 1) { ?>
                            <div class="col-md-6 latest_post--thumbnail post_thumbnail module">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="col-md-6 latest_post--content module">
                                <p class="latest_post--date"><?php the_date(); ?></p>
                                <a class="latest_post--title post_title"
                                   href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                <div class='post_category'><?php show_post_categories(); ?></div>
                                <p class="post_excerpt latest_post--excerpt"><?= get_the_excerpt(); ?></p>
                                <div class="reactions_section">
                            <span class="reaction_section--favs">
                                <svg width="13px" height="12px" viewBox="0 0 13 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g fill="none" fill-rule="evenodd">
                                        <g transform="translate(-549 -2258)">
                                            <g transform="translate(542 1865)">
                                                <g transform="translate(1 387)">
                                                    <rect width="24" height="24" opacity=".75"/>
                                                    <path d="m12.5 18-0.9425-0.86322c-3.3475-3.054-5.5575-5.0681-5.5575-7.5401 0-2.0142 1.573-3.5967 3.575-3.5967 1.131 0 2.2165 0.5297 2.925 1.3668 0.7085-0.83706 1.794-1.3668 2.925-1.3668 2.002 0 3.575 1.5826 3.575 3.5967 0 2.4719-2.21 4.4861-5.5575 7.5466l-0.9425 0.85668z" fill="#999"/>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                               17 favs
                            </span>
                                    <span class="reaction_section--comment">
                                <svg width="15px" height="14px" viewBox="0 0 15 14" version="1.1"
                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g fill="none" fill-rule="evenodd">
                                        <g transform="translate(-663 -2257)">
                                            <g transform="translate(542 1865)">
                                                <g transform="translate(1 387)">
                                                    <g transform="translate(115)">
                                                        <rect width="24" height="24" opacity=".75"/>
                                                        <path d="m18.249 14.624c0.35832-0.8348 0.55674-1.7545 0.55674-2.7205 0-3.8125-3.0906-6.9031-6.9031-6.9031-3.8125 0-6.9031 3.0906-6.9031 6.9031 0 3.8125 3.0906 6.9031 6.9031 6.9031 1.1553 0 2.2442-0.28379 3.201-0.78545l3.908 0.78161c0.54412 0.10882 0.82151-0.21824 0.61634-0.73118l-1.3791-3.4476z" fill="#999"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                                <?php comments_number() ?>
                            </span>
                                </div>
                            </div>
                        <?php } else {
                            get_template_part('content');
                        }
                    }
                }
                wp_reset_postdata();
                ?>
            </div>
            <div class="load_more w-100 text-center module">
                <button class="load_more--button"><?php _e('Load More', 'unicorn'); ?></button>
            </div>
        </div>
    </div>
<?php
get_footer();
?>