<?php
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

show_admin_bar(false);


add_action('wp_enqueue_scripts', 'uniduck_scripts');

function uniduck_scripts()
{
    wp_enqueue_style('uniduck-style', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_script('uniduck-js', get_template_directory_uri() . '/js/bundle.js', '', '', true);
    wp_localize_script('uniduck-js', 'ajax',
        array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('after_setup_theme', 'uniduckWordPressSetup');

function uniduckWordPressSetup()
{

    register_nav_menus(array(
        'primary' => __('Primary Menu')
    ));

    add_theme_support('post-thumbnails');
    add_theme_support('responsive-embeds');
    add_theme_support('post-formats', array('aside', 'gallery', 'link'));
}

function add_async_attribute($tag, $handle)
{
    $scripts_to_async = array('uniduck-js',);

    foreach ($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

function add_defer_attribute($tag, $handle)
{
    $scripts_to_defer = array('uniduck-js');

    foreach ($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }
    return $tag;
}

add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

function wp_nav_menu_no_ul()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'primary',
        'fallback_cb' => 'fall_back_menu'
    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);
}

function add_svg_to_upload_mimes($upload_mimes)
{
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}

add_filter('upload_mimes', 'add_svg_to_upload_mimes', 10, 1);

remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);


function excerpt_dots_change($more)
{
    global $post;
    remove_filter('excerpt_more', 'excerpt_dots_change');
    return '... <a class="read_more" href="' . get_permalink($post->ID) . '">' . ' Read More' . '</a>';
}

add_filter('excerpt_more', 'excerpt_dots_change', 11);

add_action('wp_ajax_load_more_function', 'load_more_function');
add_action('wp_ajax_nopriv_load_more_function', 'load_more_function');
function load_more_function()
{
    $query = new WP_Query([
        'posts_per_page' => $_POST["posts_per_page"],
        'paged' => $_POST["paged"]
    ]);
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            if ($query->current_post > 0) {
                get_template_part('content');
            }
        }
        wp_reset_query();
    }
    die();
}

function quotes_shortcode($atts)
{
    $text = $atts['text'];
    $position = $atts['position'];
    $div_position = $position == 'left' ? 'float-left' : 'float-right';
    return "<div class='quote_text " . $div_position . "'><h3>“" . $text . "”</h3></div>";
}

add_shortcode('quote', 'quotes_shortcode');

function show_post_categories()
{
    $categories = get_the_category();
    foreach ($categories as $category) {
        echo "<a href='" . get_category_link($category->term_id) . "'>$category->name</a>";
    };
}

function get_related_posts()
{
    $related = get_posts(array('numberposts' => 1, 'post__not_in' => array(get_the_ID()), 'orderby' => 'rand'));
    if ($related) foreach ($related as $post) {
        setup_postdata($post); ?>
        <div class="col-md-6">
            <?= get_the_post_thumbnail($post->ID); ?>
        </div>
        <div class="col-md-6 text-left">
            <a class="latest_post--title post_title"
               href="<?= get_the_permalink($post->ID); ?>"><?= $post->post_title ?></a>
            <p class="post_excerpt latest_post--excerpt"><?= get_the_excerpt($post->ID); ?></p>
        </div>
    <?php }
    wp_reset_postdata();
}

function custom_excerpt_length($length)
{
    return 35;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);