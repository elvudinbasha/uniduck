<?php
get_header();
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach ($query_args as $key => $string) {
    $query_split = explode("=", $string);
    $search_query[$query_split[0]] = urldecode($query_split[1]);
}
$search_posts = new WP_Query($search_query);
?>
    <div class="block block--padding">
        <div class="top__content">
            <div class="container">
                <h2 class="block__title">Search Results for: <b
                            class="search_key"><?= str_replace('%20', ' ', $query_split[1]) ?></b></h2>
                <div class="row">
                    <?php if ($search_posts->have_posts()) { ?>
                        <?php while ($search_posts->have_posts()) {
                            $search_posts->the_post();
                            get_template_part('content');
                        }
                        wp_reset_postdata();
                    } else { ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
?>