<?php
get_header();
?>
<div class="block block--padding no--template">
    <div class="container">
        <h1 class="block__title"><?= single_cat_title('Category: ', false); ?></h1>
        <div class="row">
            <?php while (have_posts()) : the_post();
                get_template_part('content');
            endwhile; ?>
        </div>
    </div>
</div>
<?php
get_footer();
?>
