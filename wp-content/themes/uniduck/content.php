<div class="col-lg-4 col-md-6 col-sm-12 post_thumbnail post_item come-in">
    <?php the_post_thumbnail(); ?>
    <p class="latest_post--date"><?= get_the_date(); ?></p>
    <a class="latest_post--title post_title" href="<?php the_permalink(); ?>"><?php the_title() ?></a>
    <div class="post_category">
        <?php $categories = get_the_category();
        foreach ($categories as $category) { ?>
            <a href="<?= get_category_link($category->term_id) ?>"><?= $category->name; ?></a>
        <?php } ?>
    </div>
    <p class="post_excerpt"><?= get_the_excerpt(); ?></p>
    <div class="reactions_section">
        <span class="reaction_section--favs">
            <svg width="13px" height="12px" viewBox="0 0 13 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g fill="none" fill-rule="evenodd">
                    <g transform="translate(-549 -2258)">
                        <g transform="translate(542 1865)">
                            <g transform="translate(1 387)">
                                <rect width="24" height="24" opacity=".75"/>
                                <path d="m12.5 18-0.9425-0.86322c-3.3475-3.054-5.5575-5.0681-5.5575-7.5401 0-2.0142 1.573-3.5967 3.575-3.5967 1.131 0 2.2165 0.5297 2.925 1.3668 0.7085-0.83706 1.794-1.3668 2.925-1.3668 2.002 0 3.575 1.5826 3.575 3.5967 0 2.4719-2.21 4.4861-5.5575 7.5466l-0.9425 0.85668z" fill="#999"/>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            17 favs
        </span>
        <span class="reaction_section--comment">
            <svg width="15px" height="14px" viewBox="0 0 15 14" version="1.1" xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink">
                <g fill="none" fill-rule="evenodd">
                    <g transform="translate(-663 -2257)">
                        <g transform="translate(542 1865)">
                            <g transform="translate(1 387)">
                                <g transform="translate(115)">
                                    <rect width="24" height="24" opacity=".75"/>
                                    <path d="m18.249 14.624c0.35832-0.8348 0.55674-1.7545 0.55674-2.7205 0-3.8125-3.0906-6.9031-6.9031-6.9031-3.8125 0-6.9031 3.0906-6.9031 6.9031 0 3.8125 3.0906 6.9031 6.9031 6.9031 1.1553 0 2.2442-0.28379 3.201-0.78545l3.908 0.78161c0.54412 0.10882 0.82151-0.21824 0.61634-0.73118l-1.3791-3.4476z" fill="#999"/>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </svg>
            <?php comments_number() ?>
        </span>
    </div>
</div>