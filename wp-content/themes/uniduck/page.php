<?php
get_header();
?>
    <div class="block block--padding no--template">
        <div class="container">
            <h1 class="block__title"><?php the_title(); ?></h1>
            <?php while (have_posts()) : the_post();
                the_content();
            endwhile; ?>
        </div>
    </div>
<?php
get_footer();
?>